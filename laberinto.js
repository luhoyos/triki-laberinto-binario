function recorrerMatriz(){
    var matrizN = [
        [0, 0, 0, 1, 1],
        [0, 0, 1, 1, 1],
        [0, 0, 1, 0, 1],
        [0, 0, 1, 0, 1],
        [0, 0, 3, 0, 0]
    ];

    
    var actuallyDirection = 'b'; //l: left, r:right, t:top, b:bottom 
    var isSolved = false;
    var actuallyPosition = [0, 4];	
    var path = [];

    while(!isSolved){
        path.push(actuallyPosition[0] + ',' + actuallyPosition[1]);
        if(matrizN[actuallyPosition[0]][actuallyPosition[1]] === 3){
            isSolved = true;
            console.log(path);
            console.log(new Date().getMilliseconds());
        } else {
            switch(actuallyDirection){
                case 'l': {
                    if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
                        actuallyDirection = 't';
                    } else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
                    } else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
                        actuallyDirection = 'b';
                    } else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
                        actuallyDirection = 'r';
                    }
                    break;
                }
                case 'r': {
                    if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
                        actuallyDirection = 'b';
                    } else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
                    } else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
                        actuallyDirection = 't';
                    } else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
                        actuallyDirection = 'l';
                    }
                    break;
                }
                case 't': {
                    if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
                        actuallyDirection = 'r';
                    } else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
                    } else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
                        actuallyDirection = 'l';
                    } else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
                        actuallyDirection = 'b';
                    }
                    break;
                }
                case 'b': {
                    if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
                        actuallyDirection = 'l';
                    } else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
                    } else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
                        actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
                        actuallyDirection = 'r';
                    } else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
                        actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
                        actuallyDirection = 't';
                    }
                    break;
                }
            }
        }
    }
}